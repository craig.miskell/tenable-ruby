# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

Gem::Specification.new do |spec|
  spec.name = 'tenable-ruby'
  spec.homepage = 'https://gitlab.com/intruder/tenable-ruby'
  spec.license = 'MIT'
  spec.summary = %Q{Unofficial Ruby library for communicating with the tenable.io API}
  spec.description = %Q{Unofficial Ruby library for communicating with the tenable.io API (also works with Nessus 6).
  You can start, stop, pause and resume scan. Get status of scans, download reports, create policies, etc.
  Based on the excellent library for interacting with Nessus https://github.com/kost/nessus_rest-ruby by https://github.com/kost. }
  spec.email = 'patrick.craston@intruder.io'
  spec.authors = ['Vlatko Kosturjak', 'Patrick Craston']
  spec.version = File.read('VERSION')
  spec.files = ["lib/tenable-ruby.rb", "lib/error/authentication_error.rb"]
end
