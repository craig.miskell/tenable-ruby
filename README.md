# tenable-ruby (based on nessus_rest)

Unofficial Ruby library for communicating with the [tenable.io](https://www.tenable.com/products/tenable-io) API 
(also works with Nessus 6)

Based on the the excellent library for interacting with Nessus [nessus_rest](https://github.com/kost/nessus_rest-ruby) 
library by [kost](https://github.com/kost)


## Usage

```ruby
require 'tenable-ruby'

n = TenableRuby::Client.new ({:access_key => 'XXX', :secret_key => 'XXX'})
qs = n.scan_quick_template('name_of_template','name_of_template','target')
scanid = qs['scan']['id']
n.scan_wait4finish(scanid)
n.report_download_file(scanid,'nessus','myscanreport.nessus')
```

## Installation

Add this line to your application's Gemfile:

    gem 'tenable-ruby'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install tenable-ruby

## Requirements

Requirements are the standard Ruby libraries for HTTPS and JSON
parsing:
```ruby
require 'uri'
require 'net/https'
require 'json'
```

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Merge Request

## Copyright
Copyright (c) 2016 Vlatko Kosturjak, 2018 Intruder Systems Ltd. See LICENSE.txt for
further details.

## Credits

Vlatko Kosturjak made initial Nessus XMLRPC library. Averagesecurityguy made initial JSON REST patches. 
Vlatko did bugfixes, gemification and few features. 
Patrick Craston added various new features to make the library compatible with tenable.io and renamed to tenable-ruby.

